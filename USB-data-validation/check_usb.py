#Purpose: Verify checksums on cloned USB drives
#Tested with python 3.4, Debian Jessie and a generic 10 port USB hub
import os
import hashlib
import functools
import textwrap

def find_files(path):
    file_list = []
    for root, directory, filenames in os.walk(path):
        for filename in filenames:
            file_list.append(os.path.join(root, filename))
    return file_list

def checksum_device(path):
    file_list = find_files(path)
    file_checksums = []
    for file_path in file_list:
        with open(file_path, mode='rb') as f:
            hasher = hashlib.sha512()
            for data_buffer in iter(functools.partial(f.read), b''):
                hasher.update(data_buffer)
        
        file_checksums.append(hasher.hexdigest())
    
    file_checksums.sort()
    master_hash = hashlib.sha512()
    for item in file_checksums:
        master_hash.update(item.encode())
    
    return master_hash.hexdigest()

def verify(path, reference_hash=''):
    master_hash = checksum_device(path)
    return master_hash == reference_hash

def get_checksum():
    return '5f8bb477a7faafeb7c71e348fc55f1af80ee07b02ed50226626ce835e5f7021cf557416669c4a73b157fa958a8f970c2b92077dae34c64734e3f59d803b280ac'

def display_follow_up_message(any_failure, one_not_found):
    if any_failure:
        follow_up_message = '''\
        
        ----- FAILURE DETECTED -----
        
        At least one device failed verification.
        
        To remedy:
        1) Unmount failed USB drive(s)
        2) Unplug and discard any failures
        3) Plug in fresh drive(s)
        4) Ensure all drives are mounted
        5) Re-run this test
        '''
    elif one_not_found:
        follow_up_message = '''\
        
        ----- Missing devices -----
        
        At least one USB port was not found.
        
        To remedy:
        1) Plug a device into each port
        2) Mount all devices
        '''
    else:
        follow_up_message = '''\
        
        ----- PERFECT! -----
        
        Hooray, every device passed :D
        '''
    print(textwrap.dedent(follow_up_message))

def verify_all_devices(device_base_path, master_checksum_of_correctness):
    devices = ['', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    width = len(device_base_path) + len(max(devices, key=len))
    any_failure = False
    one_not_found = False
    for num in devices:
        device_path = device_base_path + num
        if os.path.isdir(device_path):
            try:
                device_passed = verify(device_path, master_checksum_of_correctness)
            except OSError:
                device_passed = False
            if device_passed:
                result = "PASS"
            else:
                result = "FAIL! ---------------------"
                any_failure = True
        else:
            result = "No device found (unplugged or not mounted)"
            one_not_found = True
        print('{} - {}'.format(device_path.ljust(width), result))
    display_follow_up_message(any_failure, one_not_found)

def wait_for_exit():
    input("Press <enter> to close ")

def main():
    print()
    print("Working...")
    device_base_path = "/media/aleph/lulzbot"
    master_checksum_of_correctness = get_checksum()
    if master_checksum_of_correctness:
        verify_all_devices(device_base_path, master_checksum_of_correctness)
    else:
        if os.path.isdir(device_base_path):
            reference_checksum = checksum_device(device_base_path)
            print('Checksum for device {} is {}'.format(device_base_path, reference_checksum))
        else:
            print('No device mounted at {}'.format(device_base_path))
    wait_for_exit()

#This is a deliberate catch-all exception block
#It is designed to make it obvious to users when a problem is beyond their control
#During rework or debugging, this should probably be disabled
try:
    main()
except Exception as error:
    print('Some crazy problem happened! Contact your supervisor for help.')
    print('Technical details: ' + repr(error))
    wait_for_exit()