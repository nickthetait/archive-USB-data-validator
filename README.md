# USB Validation Tool

## About
Little python script which validates data integrity by using checksums.

## Justification
Industrial data duplication hardware are not perfect. Failure rates were higher than I expected, at least 4% of drives had some amount of data wrong. Though the failures were pretty minor - a few bits off to (worst case) a few corrupted files - this was still an unacceptable time sink.

This software automated a time intensive and somewhat manual one-by-one process done by manufacturing technicians. This saves time by testing many devices simultaneously.
Additionally improves usability and ease of training by adding some error checking and easy to understand explanations.

## Environment
Tested with python 3.4, Debian Jessie and a generic 10 port USB hub.

## Process
1) Take a fingerprint of the "golden data" you want to use
2) Use a data duplication device to produce many USB drives
3) Run this validation tool against each drive to check the data still matches
4) ???
4) Ship finished printers (aka Profit!)
